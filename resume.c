//FORK
if (fork() == 0) //Si fils
execlp("gcc", "gcc", "-c", "main.c", NULL);//Execute une commande avec 2 params
wait(&statut);//Attend que les procs fils est fini et stock le res dans statut

//PIPE
int t_nombresPairs[2]; pipe(t_nombresPairs); //Créer un pipe
#define ENTREE = 1, SORTIE = 0//Entree et sortie d'un pipe
close(t_nombresPairs[ENTREE]); //Close l'entrée (on ne peut que lire la sortie
read(t_nombresPairs[SORTIE], &n1, sizeof(n1)) //Lit un int sur la sortie

//SIGNAL
signal(SIGUSR1, myFunction); //détourne le signal SIGUSR1 vers myFunction
kill(pid, SIGUSR2); //Envoie le signal SIGSR2 vers le proces pid

//SEMAPHORE
int sem; sem = sem_create(CLE, 0); //Créer un sémaphore avec 0 jeton
sem_init(&(sem), 0, N);//pareil si library standard (N jeton)
V(sem);//Donne un jeton
sem_wait(&(sem));//Pareil si library standard
P(sem);//Prend un jeton et attend si nécéssaire
sem_post(&(sem));//Pareil si library standard
sem_delete(sem);//Supprime le sem
sem_destroy(&sem_ligne);//Pareil si librarie standard
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;//Créer un mutex
pthread_mutex_unlock(&mutex);//Dévérouille un mutex
pthread_mutex_destroy(&mutex);//Supprime un mutex

//SHARE MEMORY
shmid = create_shm(CLE, SIZE_BUFFER, 0);//Réserve la mémoire
char *buffer; buffer = shmat(shmid, 0, 0));//Link la mémoire à notre var buffer
buffer[i] = 'a';//écrit un a dans le buffer à l'indice i
shmctl(shmid, IPC_RMID, 0)//Supprime la mémoire partagée

//THREAD
pthread_t th; thread_create(&(thProd[i]), NULL, prod, param)
//^Créer un thread qui lance la function prod avec un param (NULL par défaut)
void* prod (void* param)//Proto obligatoire de la fcn thread
pthread_exit(0);//Termine un thread à mettre à la fin de la fcn
pthread_join(th, NULL); //Attend que le thread est fini de s'executer
