#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>

int fin = 0;

void myFunction(int signal) {
  fin =1;
}

int main(){
  int i, n=0;
  pid_t fils_pid;
  for(i=1;i<5;i++){
   fils_pid = fork();
   if(fils_pid>0){
     signal(SIGCHLD, myFunction);
     while (!fin) {
      ;
     }
     n=i*2;
     break;
   }
  }
  printf("%d\n",n);
  return 0;
}
