#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
/*
  if (fork() == 0)
    if (fork() == 0)
      execlp("ps", "ps", NULL);
    else
      execlp("who", "who", NULL);
  else
    execlp("ls", "ls", "-l", NULL);
 */

  int statut;
  if (fork() == 0)
    execlp("who", "who", NULL);
  wait(&statut);
  
  if (fork() == 0)
    execlp("ls", "ls", "-l", NULL);
  wait(&statut);
  
  if (fork() == 0)
    execlp("ps", "ps", NULL);
  wait(&statut);
  return (0);  
}