#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

#define N 5

struct prodcons {
  int buffer[N];
  int posLec, posEcr;
  sem_t semLecture; //nb d'element dispo dans le buffer
  sem_t semEcriture; //nb de places dispo dans le buffer
};

pthread_mutex_t accesBuffer = PTHREAD_MUTEX_INITIALIZER;
struct prodcons usine;

void *prod(void *arg) {
  sem_wait(&(usine.semEcriture));
  pthread_mutex_lock(&accesBuffer);
  usine.buffer[usine.posEcr] = 6;
  usine.posEcr = (usine.posEcr + 1) % N;
  pthread_mutex_unlock(&accesBuffer);
  sem_post(&(usine.semLecture));
  pthread_exit(0);
}

void *conso(void *arg) {
  sem_wait(&(usine.semLecture));
  pthread_mutex_lock(&accesBuffer);
  printf("lu : %d \n", usine.buffer[usine.posLec]);
  usine.posLec = (usine.posLec + 1) % N;
  
  pthread_mutex_unlock(&accesBuffer);
  sem_post(&(usine.semEcriture));
  pthread_exit(0);
}


int main(int argc, char* argv[]) {
  int i;
  int nbProd = atoi(argv[1]), nbConso = atoi(argv[2]); 
  pthread_t thProd[nbProd], thConso[nbConso];
  
  sem_init(&(usine.semLecture), 0, 0);
  sem_init(&(usine.semEcriture), 0, N);
  
  usine.posEcr = 0;
  usine.posLec = 0;
  
  if(argc == 3) {
    for(i = 0; i< nbProd; i++) {
      if (pthread_create(&(thProd[i]), NULL, prod, NULL) != 0) {
	perror("Prod thread creation error\n");
	exit(-1);
      }
    }
    for(i=0; i< nbConso; i++) {
      if (pthread_create(&(thConso[i]), NULL, conso, NULL) != 0) {
	perror("Conso thread creation error\n");
	exit(-1);
      }
    }
  }
  
  //joins
  for(i = 0; i< nbProd; i++) {
    pthread_join(thProd[i], NULL);
  }
  for(i=0; i< nbConso; i++) {
    pthread_join(thConso[i], NULL);
  }
    
  return 0;
}