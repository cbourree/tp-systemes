#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>

#define ENTREE 1
#define SORTIE 0

int main(int argc, char* argv[]){
  int N = atoi(argv[1]);
  int i, nGenere, nRecu, tubes[N][2], pidRecu, indRecu, myPID, nGeneres[N], nextTube;
  
  srand(time(NULL));
  
  for (i = 0; i < N; i++)
    nGeneres[i] = rand() % 500 + 1;
  
  for (i = 0; i < N; i++) 
    pipe(tubes[i]);
  
  for (i = 0; i < N; i++) {
   if (fork() == 0) {
     nGenere = nGeneres[i];
     
     if (i > 0) {
       //printf("%d : Je suis langfdsgdsgdsgds\n", i);
       read(tubes[i-1][SORTIE], &nRecu, sizeof(nRecu));
       read(tubes[i-1][SORTIE], &pidRecu, sizeof(pidRecu));
       read(tubes[i-1][SORTIE], &indRecu, sizeof(indRecu));
       
       if (i < N)
	 nextTube = i;
       else
	 nextTube = 0;
       
       if (nGenere < nRecu) {
	 write(tubes[nextTube][ENTREE], &nRecu, sizeof(nRecu));
	 write(tubes[nextTube][ENTREE], &pidRecu, sizeof(pidRecu));
	 write(tubes[nextTube][ENTREE], &indRecu, sizeof(indRecu));
       } else {
	 myPID = getpid();
	 write(tubes[nextTube][ENTREE], &nGenere, sizeof(nGenere));
	 write(tubes[nextTube][ENTREE], &myPID, sizeof(myPID));
	 write(tubes[nextTube][ENTREE], &i, sizeof(i));
       }
     } else { //process 0

       myPID = getpid();
       write(tubes[0][ENTREE], &nGenere, sizeof(nGenere));
       write(tubes[0][ENTREE], &myPID, sizeof(myPID));
       write(tubes[0][ENTREE], &i, sizeof(i));
       
       read(tubes[N - 1][SORTIE], &nRecu, sizeof(nRecu));
       read(tubes[N - 1][SORTIE], &pidRecu, sizeof(pidRecu));
       read(tubes[N - 1][SORTIE], &indRecu, sizeof(indRecu));
     
     }
     printf("Je suis le n°%d, pid : %d, nGenere : %d\n\tn:%d Et j'ai reçu du n°%d, pid : %d, la valeur %d\n", i, getpid(), nGenere, i, indRecu, pidRecu, nRecu);

     break;
   }
  }
  return 0;
}