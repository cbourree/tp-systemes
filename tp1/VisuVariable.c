#include <stdio.h>
#include <string.h>

extern char **environ;


char *strupr(char *str) {
  int i = 0;

  while (str[i] != '\0') {
    str[i] = toupper(str[i]);
    i++;
  }
  return (str);
}

int main(int argc, char *argv[]) {
  int i = 0;
  
  if (argc == 2) {
    while (environ[i] != 0) {
      if (strncmp(strupr(argv[1]), strupr(environ[i]), strlen(argv[1])) == 0) {
	printf("%s\n", environ[i]);
      }
      i++;
    }
  } else {
    printf("Nombre d'argument invalide\n");
  }
 
  return (0);
}