#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
  int i, delai, statut;
  
  for (i = 0; i < 4; i++) if (fork()) break;
  
  srand(getpid());
  delai = rand() % 4;
  sleep(delai);
  wait(NULL);
  printf("Mon nom est %c, j'ai dormi %d secondes \n", 'A' + i, delai);
  exit(0);
 
 
}