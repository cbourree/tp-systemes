#include <stdio.h>

int myAtoi(char *str) {
    int res = 0; // Initialize result
    int i;
  
    // Iterate through all characters of input string and
    // update result
    for (i = 0; str[i] != '\0'; ++i)
        res = res*10 + str[i] - '0';
  
    // return result.
    return res;
}

int main(int argc, char *argv[]) {
  int i;
  int somme = 0;
  int nb;
  
  if (argc < 2) {
    printf("Aucune moyenne a calculer !\n");
  } else {
    for (i = 1; i < argc; i++) {
      nb = myAtoi(argv[i]);
      if (nb < 0 || nb > 20) {
	printf("Note non valide !\n");
	return (-1);
      }
      somme += nb;
    }
    printf("Resultat : %2.2f \n", somme / (argc - 1.0));
  }
  return (0);
}
