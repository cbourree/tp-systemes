#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "dijkstra.h"

#define CLE 123

int main() {
  int sum;
  sum = sem_create(CLE,0);
  
  if (fork() == 0){
    printf("tachea...\n");
    P(sum);
    printf("TACHE 2\n");
    printf("tachea...\n");
  } else {
    printf("tacheb...\n");
    printf("TACHE 1\n");
    sleep(2);
    V(sum);
    printf("tacheb...\n");
    printf("tacheb...\n");    
    wait();
    sem_delete(sum);    
  }
  
  return 0;
}