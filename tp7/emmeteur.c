#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "dijkstra.h"

#define CLE 123
#define N 5

void rdv1(){
 printf("l'emmeteur arrive au rdv\n");
}

int main() {
  int fromReceteur, fromEmmeteur, i;
  fromReceteur = sem_create(CLE, 0);
  fromEmmeteur = sem_create(CLE+1, 0);
  

  for (i = 0; i < N; i++) {
    P(fromReceteur);
  }
  for (i = 0; i < N; i++) {
    V(fromEmmeteur);
  }
  
  rdv1();
  
  sleep(1);
    sem_delete(fromReceteur);
    sem_delete(fromEmmeteur);
  
  return 0;
}