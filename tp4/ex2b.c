#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

int main(int argc, char* argv[]){
  int tube[2], entree, sortie, tube2[2];
  pipe(tube);
  
  entree = 1;
  sortie = 0;
  
  if (fork() == 0) { //fils
    close(tube[sortie]);
    dup2(open("fichier", O_RDONLY), 0); // la source de lecture du fils devient l"extremité lecture du tube
    dup2(tube[entree], 1);
    execlp("sort", "sort", NULL);
    //close(tube[entree]);
  } else {
    pipe(tube2);
    if (fork() == 0) { //Deuxieme fils
      close(tube[entree]);
      close(tube2[sortie]);
      dup2(tube[sortie], 0);
      dup2(tube2[entree], 1);
      close(tube[sortie]);
      close(tube2[entree]);
      execlp("grep", "grep", "s", NULL);
    } else {
      close(tube[entree]);
      close(tube[sortie]);
      close(tube2[entree]);
      dup2(tube2[sortie], 0);
      dup2(open("sortie", O_WRONLY | O_CREAT), 1);
      close(tube2[sortie]);
      execlp("tail", "tail", "-n", "2", NULL);
    }
    
  }
  
  return 0;
}