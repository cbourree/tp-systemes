#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>


void myFunction(int signal) {
  printf("On m'a demandé de m'arrêter...\n");
  exit(0);
}

int main(int argc, char* argv[]){
  
  signal(SIGINT, myFunction);
  
  while (1) {
    ;
  }
  return 0;
}