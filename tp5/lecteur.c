#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

int main(int argc, char* argv[]){
  int tube, longueur;
  char message[50];
  tube = open("TubeNomme", O_RDONLY);
  longueur = read(tube, message, 30);
  message[longueur]='\0';
  printf("%d a lu le message \n \t \t%s\n", getpid(), message);
  close(tube);
  
  return 0;
}