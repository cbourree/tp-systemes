#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "shared.h"
#include "dijkstra.h"

int main() {
  int shmid;
  int sem_protec_tableau, sem_prod_possible, sem_donnee_a_lire;
  
  //get shm
  shmid = create_shm(CLE, SIZE_BUFFER + 2, 0);
  
  
  //clear shm
  if (shmctl(shmid, IPC_RMID, 0) == -1) {
      perror("Destruction impossile\n");
      exit(1);
  }

  //clear semas
  sem_protec_tableau = sem_create(CLE + 1, 0);
  sem_prod_possible = sem_create(CLE + 2, SIZE_BUFFER);
  sem_donnee_a_lire = sem_create(CLE + 3, 0);
  sem_delete(sem_protec_tableau);
  sem_delete(sem_prod_possible);
  sem_delete(sem_donnee_a_lire);
  
  printf("memoire partagee supprimee: cle: %d, ID: %d\n", CLE, shmid);
  
  return 0;
}