#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "dijkstra.h"

#define CLE 123
#define N 5

void rdv1(){
 printf("recepteur %d arrive au rdv\n", getpid());
}

int main() {
  int fromReceteur, fromEmmeteur, i;
  fromReceteur = sem_create(CLE, 0);
  fromEmmeteur = sem_create(CLE+1, 0);
  

  V(fromReceteur);
  P(fromEmmeteur);
  rdv1();
  
  return 0;
}