#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
  int i, pid_fils, etat;
  
  for (i = 0; i < atoi(argv[1]); i++) {
    if (fork() == 0) {
      printf("# getpid : %d, getppid : %d\n", getpid(), getppid());
      sleep(2*i);
      printf("# (fin attente) getpid : %d, getppid : %d\n", getpid(), getppid());
      exit(i);
    }
  }
  
  pid_fils = 0;
  while(pid_fils != -1) {
    pid_fils = wait(&etat);
    printf("$ pid_fils : %d, etat : %d \n", pid_fils, etat);
  }
}