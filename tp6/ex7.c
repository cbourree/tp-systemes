#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>

int leFilsPeutAfficher = 0;

void myFunction(int signal) {
  //printf("signal : %d\n", signal);
  if (signal == SIGUSR1){
    leFilsPeutAfficher = 1;
  } else {
    leFilsPeutAfficher = 0;
  }  
}

int main(){
  int i, pid;
  pid = fork();
  if(pid == 0) {
    signal(SIGUSR1, myFunction);
    signal(SIGUSR2, myFunction);
    while(1){
      while(leFilsPeutAfficher){
	printf("$$$$$$$$$$$$$$$$$\n");
      }      
    }
  } else {
    for(i = 0; i<7; i++){
      if (i == 3 || i == 5){
	//printf("pid à qui on envoie : %d\n", pid);
	kill(pid, SIGUSR1);
      }
      printf("%d !!!!!!!!!!!!!!!!!\n", i);
      sleep(1);
      kill(pid, SIGUSR2);
    }
  }
  
  
  return 0;
}