#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>

int fin = 0;

void myFunction(int signal) {
  fin = 1;
}

int main(int argc, char* argv[]){
  
  signal(SIGINT, myFunction);
  
  while (!fin) {
    ;
  }
  return 0;
}