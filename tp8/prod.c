#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "dijkstra.h"
#include "shared.h"

int main(int argc, char* argv[]) {
  char *buffer;
  int sem_protec_tableau, sem_prod_possible, sem_donnee_a_lire;
  int shmid;
  int pE;
  
  if (argc < 2)
    exit(1);
  
  //get semaphores
  sem_protec_tableau = sem_create(CLE + 1, 0);
  sem_prod_possible = sem_create(CLE + 2, SIZE_BUFFER);
  sem_donnee_a_lire = sem_create(CLE + 3, 0);
  
  //get shm id 
  shmid = create_shm(CLE, SIZE_BUFFER + 2, 0);
  
  //Attribution shm to buffer
  if ((buffer = shmat(shmid, 0, 0)) == (char *)-1) {
    perror("Attahchement impossible (buffer)\n");
    exit(1);
  }
  
  //production
  P(sem_prod_possible);
  P(sem_protec_tableau);
  pE = buffer[SIZE_BUFFER];
  buffer[SIZE_BUFFER] = (buffer[SIZE_BUFFER] + 1) % SIZE_BUFFER;
  buffer[pE] = argv[1][0];
  V(sem_protec_tableau);
  V(sem_donnee_a_lire);
  
  return 0;
}