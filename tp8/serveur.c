#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <stdio.h>
#include "dijkstra.h"

#define N 5
#define CLE 15

int main() {
  int* id;
  int i, mon_id, sem_fin, sem_id, shmid;
  
  shmid = create_shm(CLE, sizeof(int), IPC_CREAT | IPC_EXCL | 0777); // ?
    
  if ((id = shmat(shmid, NULL, 0)) == (int*)-1) {
    perror("shmat");
    exit(1);
  }
  
  sem_fin = sem_create(CLE+2, 0);
  sem_id = sem_create(CLE+1, 1);
  
  P(sem_id);
  *id = *id +1;
  mon_id = *id;	
  V(sem_id);
  
  if (mon_id >= N) {
    for(i = 0; i < N; i++) {
      V(sem_fin);
    }
  }
  
  P(sem_fin);
  //cmd
  printf("Server id : %d\n", mon_id);
  
  //clear shm & sem ?
  if (mon_id >= N) {
    if (shmctl(shmid, IPC_RMID, 0) == -1) {
      perror("Destruction impossile\n");
      exit(1);
    }
    sem_delete(sem_fin);
    sem_delete(sem_id);
  }
  return 0;
}