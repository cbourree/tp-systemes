#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

sem_t sem;
int i;

void *traitement(void * arg) {
  if (*(int *) arg == 2) {
    sem_wait(&sem);
  }
  for (; i < 100; i++) {
    printf("%d)Traitement du thread %d\n", i, *(int *)arg);
    if (i == 70) {
      i++;
      break;
    }
  }
  if (*(int *) arg == 1) {
    sem_post(&sem);
  }
  pthread_exit(0);
}

int main(void) {  
  pthread_t th1, th2;
  void *retour;
  int nb1 = 1, nb2 = 2;
  sem_init(&sem, 0, 0);
  
  i = 0;
  
  if (pthread_create(&th1, NULL, traitement, &nb1) != 0) {
    perror("Premier thread create\n");
    exit(-1);
  }
  
  if (pthread_create(&th2, NULL, traitement, &nb2) != 0) {
    perror("Deuxieme thread create\n");
    exit(-1);
  }
  
  
  pthread_join(th1, &retour);
  pthread_join(th2, &retour);
  sem_destroy(&sem);
  return 0;
}