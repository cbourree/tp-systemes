#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>

#define ENTREE 1
#define SORTIE 0
#define N 5

int main(int argc, char* argv[]){
  int t_nombresPairs[2], t_nombresImpairs[2], 
    t_sommePairs[2], t_sommeImpairs[2];
  int n1, i, somme = 0;
  
  pipe(t_nombresPairs);
  pipe(t_sommePairs);
  
  if (fork() == 0) { //fils1  (filtre pair)
    close(t_sommePairs[SORTIE]);
    close(t_nombresPairs[ENTREE]);
    for (i = 0; i < N; i++) {
      if (read(t_nombresPairs[SORTIE], &n1, sizeof(n1))) {
	printf("$%d\n", n1);
	somme += n1;
      }
    }
    
    close(t_nombresPairs[SORTIE]);
    write(t_sommePairs[ENTREE], &somme, sizeof(somme));
    
    close(t_sommePairs[ENTREE]);
      
  } else {
    pipe(t_nombresImpairs);
    pipe(t_sommeImpairs); 
    if (fork() == 0) { //fils2 (filtre impaire)
      close(t_sommePairs[ENTREE]);
      close(t_sommePairs[SORTIE]);
      close(t_nombresPairs[ENTREE]);
      close(t_nombresPairs[SORTIE]);
      close(t_nombresImpairs[ENTREE]);
      close(t_sommeImpairs[SORTIE]);
      for (i = 0; i < N; i++) {
	if (read(t_nombresImpairs[SORTIE], &n1, sizeof(n1))) {
	  printf("£%d\n", n1);
	  somme += n1;
	}
      }
      
      close(t_nombresImpairs[SORTIE]);
      write(t_sommeImpairs[ENTREE], &somme, sizeof(somme));
      
      close(t_sommeImpairs[ENTREE]);
    } else { //(générateur)
      close(t_sommePairs[ENTREE]);
      close(t_sommeImpairs[ENTREE]);
      close(t_nombresPairs[SORTIE]);
      close(t_nombresImpairs[SORTIE]);
      srand(time(NULL));
      for (i = 0; i < N; i++) {
	n1 = rand() % 100 + 1;
	if (n1 % 2 == 0) {//Nombre pair
	  printf("->%d\n", n1);
	  write(t_nombresPairs[ENTREE], &n1, sizeof(n1));
	} else { //Nombre impaires
	  printf("->%d\n", n1);
	  write(t_nombresImpairs[ENTREE], &n1, sizeof(n1));
	}
      }
      
      close(t_nombresPairs[ENTREE]);
      close(t_nombresImpairs[ENTREE]);
      
      read(t_sommePairs[SORTIE], &somme, sizeof(somme));
      printf("Somme pairs = %d\n", somme);
      close(t_sommePairs[SORTIE]);
      
      read(t_sommeImpairs[SORTIE], &somme, sizeof(somme));
      printf("Somme impairs = %d\n", somme);
      close(t_sommeImpairs[SORTIE]);
    }
  }
  
  return 0;
}