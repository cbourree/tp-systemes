#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

int main(int argc, char* argv[]){
  int tube[2], pid;
  unsigned char buffer[3];
  int i;
  
  printf("Création du tube en tant que %d fils de %d\n", getpid(), getppid());
  if (pipe(tube) != 0) {
    perror("Problème - Création du tube\n");
    return(1);
  }
  for(i=0; i<256; i++) buffer[i] = i;
  printf("Ecriture dans le tube en tant que %d fils de %d\n", getpid(), getppid());
  if (write(tube[1], buffer, 256) != 256){
    perror("Problème - Ecriture dans le tube\n");
    return(1);
  }
  if (fork() == 0) { //fils
    close(tube[1]);  
    printf("Lecture depuis le tube en tant que %d fils de %d\n", getpid(), getppid());
    if (read(tube[0], buffer, 256) != 256){
      perror("Problème - Lecture depuis le tube\n");
      return(1);
    }
    close(tube[0]);
  } else { //père
    close(tube[0]);
    close(tube[1]);
  }
  return 0;
}