#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>

#define N 5

int main(int argc, char* argv[]) {
  int i, pid4, pid2;
  for (i = 0; i < N; i++) {
      fork();
      fork();
  }
  printf("Bonjour !\n");
  return (0);
}