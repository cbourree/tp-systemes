#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>

int main(int argc, char* argv[]){
  int tube;
  char message[50];
  //strcpy(message, "jean-eude le fdp");
  sprintf(message, "processus %d ", getpid());
  tube = open("TubeNomme", O_WRONLY);
  write(tube, message, strlen(message));
  close(tube);
  
  return 0;
}