#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <pthread.h>

void* traitement (void* arg) {
  FILE* fp1, *fp2;
  int c;
  char *f;
  
  f = (char*) arg;
  
  fp1 = fopen(f, "r");
  fp2 = fopen(f, "r+");
  
  if((fp1 == NULL) || (fp2 == NULL)) {
    perror("fopen");
    exit(1);
  }
  
  while(c != EOF) {
    c = fgetc(fp1);
    if(c != EOF) {
      fputc(toupper(c), fp2);
    }
  }
  
  fclose(fp1);
  fclose(fp2);
  
  pthread_exit(0);
}

int main (int argc, char** argv) {
  pthread_t threads[argc-1];
  int i;
  
  for(i = 1; i < argc; i++) {
    if (pthread_create(&(threads[i-1]), NULL, traitement, argv[i]) != 0) {
      perror("thread create\n");
      exit(-1);
    }
  }
  
  for(i = 0; i < argc; i++) {
    pthread_join(threads[i], NULL);
  }
  
  return 0;
}