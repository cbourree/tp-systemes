#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "dijkstra.h"
#include "shared.h"

int main() {
  char *buffer;
  int shmid;
  int sem_protec_tableau; //semaphore d'acces au buffer
 
 //creation mem partagée de taille spécifiée + 2 emplacements pour pE & pL
  shmid = shmget(CLE, SIZE_BUFFER + 2, IPC_CREAT | IPC_EXCL | 0777);
  if (shmid == -1) {
    shmid = create_shm(CLE, SIZE_BUFFER + 2, 0);
    if (shmctl(shmid, IPC_RMID, 0) == -1) {
      perror("Destruction impossile\n");
      exit(1);
    }
    shmid = create_shm(CLE, SIZE_BUFFER + 2, IPC_CREAT | IPC_EXCL | 0777);
  }
  
  //Attribution mem partagée à buffer
  buffer = shmat(shmid, NULL, 0);
  if (buffer == (char*)-1) {
    perror("Attahchement impossible (buffer)\n");
    exit(1);
  }
  
  //Création semaphores
  sem_protec_tableau = sem_create(CLE + 1, 1);
  sem_create(CLE + 2, SIZE_BUFFER);
  sem_create(CLE + 3, 0);
  
  //Init pE & pL
  P(sem_protec_tableau);
  buffer[SIZE_BUFFER] = 0;
  buffer[SIZE_BUFFER + 1] = 0;
  V(sem_protec_tableau);

  printf("memoire partagee: cle: %d, ID: %d\n", CLE, shmid);
  
  return 0;
}