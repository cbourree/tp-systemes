#include <sys/types.h>

int sem_create(key_t  cle, int  initval);

void P(int semid);

void V(int semid);

void sem_delete(int semid);

int create_shm(key_t cle, int size, int flag);