#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>

void myFunction(int signal) {
  printf("On m'a demandé de m'arrêter...\n");
}

int main(){
  int i, pid;
  pid = fork();
  if(pid == 0) {
    signal(SIGINT, myFunction);
    while(1){
      printf("$$$$$$$$$$$$$$$$$\n");
    }
  } else {
    for(i = 0; i<4; i++){
      printf("%d !!!!!!!!!!!!!!!!!\n", i);
      sleep(1);
    }
  }
  
  
  return 0;
}