#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>

int main(int argc, char* argv[]){
  int pfd[2], pid;
  pipe(pfd);
  pid = fork();
  if (pid == 0) {//fils
    close(pfd[1]); // pour le fils on ferme l'écriture
    dup2(pfd[0], 0); // en tant que wc, ma source de lecture (0) devient l'extremité "lecture" du tube (sortie)
    close(pfd[0]);
    execlp("wc", "wc", NULL);
  } else {
    close(pfd[0]); // on ferme la lecture pour le père
    dup2(pfd[1], 1); //on écrase la sortie du père par l'entrée du tube
    close(pfd[1]);
    execlp("cat", "cat", "fichier", NULL);    
  }
  
  return 0;
}