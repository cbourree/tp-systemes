#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>


int tempsEcouler = 0;


void myFunction(int signal) {
  if (signal == SIGUSR1) {  
    printf("Trop tard !!\n");
    exit(0);
  }
}

int main(){
  int value;

  if (fork() == 0) {
    sleep(5);
    kill(getppid(), SIGUSR1);
  } else {
    signal(SIGUSR1, myFunction);
     printf("Merci de saisir un nombre :\n");
  
    while (scanf("%d", &value) == 0) { ; }
    
      printf("Value : %d\n", value);
  }
  
  
 
  
  return 0;
}