#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>

int main(){
  int i, pid;
  pid = fork();
  if(pid == 0) {
    while(1){
      printf("$$$$$$$$$$$$$$$$$\n");
    }
  } else {
    for(i = 0; i<4; i++){
      if(i == 3){
	kill(pid, SIGKILL);
      }
      printf("%d !!!!!!!!!!!!!!!!!\n", i);
      sleep(1);
    }
  }
  
  
  return 0;
}