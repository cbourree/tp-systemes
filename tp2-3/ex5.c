#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/wait.h>

int main(int argc, char* argv[]) {
  int i, statut;
  int fail = 0;
  
  for (i = 1; i < argc; i++)
    if (fork() == 0)
      execlp("gcc", "gcc", "-c", argv[i], NULL);     
  
  for (i = 1; i < argc; i++) {  
    wait(&statut);

    if (WIFEXITED(statut))
      printf("Terminaison normal du proc' fils\n");
    else {
      printf("Terminsaison anormal du proc' fils\n");
      fail = 1;
      return (0);
    }
  }
  
  if (fail == 0) {
    if (fork() == 0) {
      char *arguments[10];
      arguments[0] = "gcc";
      for (i = 1; i < argc; i++) {
	arguments[i] = argv[i];
	arguments[i][strlen(argv[i]) - 1] = 'o';
	printf(">---%s---<\n", arguments[i]);
      }
      //printf(">>>>%d<---\n", i);
      arguments[i] = "-o";
      arguments[i + 1] = "programme";
      arguments[i + 2] = NULL;
      //= {"gcc", "ex1.o","ex2.o","-o", "programme", NULL};
      execvp("gcc", arguments);
    }
  }
  wait(&statut);
  return (0);  
}