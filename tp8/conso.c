#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "shared.h"
#include "dijkstra.h"

int main() {
  char *buffer;
  int sem_protec_tableau, sem_prod_possible, sem_donnee_a_lire;
  int shmid;
  int pL;
  char c;
  
  //get semaphores
  sem_protec_tableau = sem_create(CLE + 1, 0);
  sem_prod_possible = sem_create(CLE + 2, SIZE_BUFFER);
  sem_donnee_a_lire = sem_create(CLE + 3, 0);
  
  //get shm
  shmid = create_shm(CLE, SIZE_BUFFER + 2, 0);
  
  //Attribution shm to buffer
  if ((buffer = shmat(shmid, 0, 0)) == (char *)-1) {
    perror("Attahchement impossible (buffer)\n");
    exit(1);
  }
  
  //consommation
  P(sem_donnee_a_lire);
  P(sem_protec_tableau);
  pL = buffer[SIZE_BUFFER + 1];
  buffer[SIZE_BUFFER + 1] = (buffer[SIZE_BUFFER + 1] + 1) % SIZE_BUFFER;
  V(sem_protec_tableau);
  c = buffer[pL];
  V(sem_prod_possible);
  
  printf("%c\n", c);
  return 0;
}