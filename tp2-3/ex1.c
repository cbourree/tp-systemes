#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
 
  printf("Début du main\n");
  if (!fork())
    printf("Traitement du fils\n");
  else
    printf("Traitement du père\n");
  printf("Traitement commun\n");
  execlp("ls", "ls", NULL);
  printf("Fin du main\n");
  
  return (0);
}