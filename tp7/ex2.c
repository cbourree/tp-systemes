#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "dijkstra.h"

#define CLE 123

void rdv1(){
 printf("le rdv 1 a eu lieu\n");
}

void rdv2(){
 printf("le rdv 2 a eu lieu\n"); 
}

void rdv3(){
 printf("le rdv 3 a eu lieu\n"); 
}

int main() {
  int sum, semy, zaza;
  sum = sem_create(CLE, 0);
  semy = sem_create(CLE+1, 0);
  zaza = sem_create(CLE+2, 0);
  
  if (fork() == 0){
    if (fork() == 0){
      sleep(5);
      V(sum);
      V(sum);
      P(semy);
      P(zaza);
      rdv1();
    } else {
      sleep(2);
      V(semy);
      V(semy);
      P(sum);
      P(zaza);
      rdv2();
    }    
  } else {    
    sleep(7);
    V(zaza);
    V(zaza);
    P(sum);
    P(semy);
    rdv3();
    
    sem_delete(sum);
    sem_delete(semy);
  }
  
  return 0;
}